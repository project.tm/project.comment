<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (!CModule::IncludeModule("blog") or ! CModule::IncludeModule("project.comment")) {
    ShowError(GetMessage("BLOG_MODULE_NOT_INSTALL"));
    return;
}

$arParams["MESSAGE_PER_PAGE"] = IntVal($arParams["MESSAGE_PER_PAGE"]) > 0 ? IntVal($arParams["MESSAGE_PER_PAGE"]) : 15;
$arParams["PREVIEW_WIDTH"] = IntVal($arParams["PREVIEW_WIDTH"]) > 0 ? IntVal($arParams["PREVIEW_WIDTH"]) : 100;
$arParams["PREVIEW_HEIGHT"] = IntVal($arParams["PREVIEW_HEIGHT"]) > 0 ? IntVal($arParams["PREVIEW_HEIGHT"]) : 100;
$arParams["SORT_BY1"] = (strlen($arParams["SORT_BY1"]) > 0 ? $arParams["SORT_BY1"] : "DATE_PUBLISH");
$arParams["SORT_ORDER1"] = (strlen($arParams["SORT_ORDER1"]) > 0 ? $arParams["SORT_ORDER1"] : "DESC");
$arParams["SORT_BY2"] = (strlen($arParams["SORT_BY2"]) > 0 ? $arParams["SORT_BY2"] : "ID");
$arParams["SORT_ORDER2"] = (strlen($arParams["SORT_ORDER2"]) > 0 ? $arParams["SORT_ORDER2"] : "DESC");
$arParams["MESSAGE_LENGTH"] = (IntVal($arParams["MESSAGE_LENGTH"]) > 0) ? $arParams["MESSAGE_LENGTH"] : 100;
$arParams["BLOG_URL"] = preg_replace("/[^a-zA-Z0-9_-]/is", "", Trim($arParams["BLOG_URL"]));
$arParams["USE_SOCNET"] = ($arParams["USE_SOCNET"] == "Y") ? "Y" : "N";
$arParams["NAV_TEMPLATE"] = (strlen($arParams["NAV_TEMPLATE"]) > 0 ? $arParams["NAV_TEMPLATE"] : "");
if (!is_array($arParams["GROUP_ID"]))
    $arParams["GROUP_ID"] = array($arParams["GROUP_ID"]);
foreach ($arParams["GROUP_ID"] as $k => $v)
    if (IntVal($v) <= 0)
        unset($arParams["GROUP_ID"][$k]);

if ($arParams["CACHE_TYPE"] == "Y" || ($arParams["CACHE_TYPE"] == "A" && COption::GetOptionString("main", "component_cache_on", "Y") == "Y"))
    $arParams["CACHE_TIME"] = intval($arParams["CACHE_TIME"]);
else
    $arParams["CACHE_TIME"] = 0;
$arParams["DATE_TIME_FORMAT"] = trim(empty($arParams["DATE_TIME_FORMAT"]) ? $DB->DateFormatToPHP(CSite::GetDateFormat("FULL")) : $arParams["DATE_TIME_FORMAT"]);

if (strLen($arParams["BLOG_VAR"]) <= 0)
    $arParams["BLOG_VAR"] = "blog";
if (strLen($arParams["PAGE_VAR"]) <= 0)
    $arParams["PAGE_VAR"] = "page";
if (strLen($arParams["USER_VAR"]) <= 0)
    $arParams["USER_VAR"] = "id";
if (strLen($arParams["POST_VAR"]) <= 0)
    $arParams["POST_VAR"] = "id";

$arParams["PATH_TO_BLOG"] = trim($arParams["PATH_TO_BLOG"]);
if (strlen($arParams["PATH_TO_BLOG"]) <= 0)
    $arParams["PATH_TO_BLOG"] = htmlspecialcharsbx($APPLICATION->GetCurPage() . "?" . $arParams["PAGE_VAR"] . "=blog&" . $arParams["BLOG_VAR"] . "=#blog#");

$arParams["PATH_TO_BLOG_CATEGORY"] = trim($arParams["PATH_TO_BLOG_CATEGORY"]);
if (strlen($arParams["PATH_TO_BLOG_CATEGORY"]) <= 0) {
    if (strlen($arParams["PATH_TO_BLOG"]) > 0)
        $arParams["PATH_TO_BLOG_CATEGORY"] = $arParams["PATH_TO_BLOG"] . "?category=#category_id#";
    if (strlen($arParams["PATH_TO_GROUP_BLOG"]) > 0)
        $arParams["PATH_TO_GROUP_BLOG_CATEGORY"] = $arParams["PATH_TO_GROUP_BLOG"] . "?category=#category_id#";

    if (strlen($arParams["PATH_TO_BLOG_CATEGORY"]) <= 0)
        $arParams["PATH_TO_BLOG_CATEGORY"] = htmlspecialcharsbx($APPLICATION->GetCurPage() . "?" . $arParams["PAGE_VAR"] . "=blog&" . $arParams["BLOG_VAR"] . "=#blog#&category=#category_id#");
}

$arParams["PATH_TO_SMILE"] = strlen(trim($arParams["PATH_TO_SMILE"])) <= 0 ? false : trim($arParams["PATH_TO_SMILE"]);

$arParams["PATH_TO_POST"] = trim($arParams["PATH_TO_POST"]);
if (strlen($arParams["PATH_TO_POST"]) <= 0)
    $arParams["PATH_TO_POST"] = htmlspecialcharsbx($APPLICATION->GetCurPage() . "?" . $arParams["PAGE_VAR"] . "=post&" . $arParams["BLOG_VAR"] . "=#blog#&" . $arParams["POST_VAR"] . "=#post_id#");

$arParams["PATH_TO_USER"] = trim($arParams["PATH_TO_USER"]);
if (strlen($arParams["PATH_TO_USER"]) <= 0)
    $arParams["PATH_TO_USER"] = htmlspecialcharsbx($APPLICATION->GetCurPage() . "?" . $arParams["PAGE_VAR"] . "=user&" . $arParams["USER_VAR"] . "=#user_id#");
// activation rating
CRatingsComponentsMain::GetShowRating($arParams);

$arParams["SHOW_LOGIN"] = $arParams["SHOW_LOGIN"] != "N" ? "Y" : "N";
$arParams["IMAGE_MAX_WIDTH"] = IntVal($arParams["IMAGE_MAX_WIDTH"]);
$arParams["IMAGE_MAX_HEIGHT"] = IntVal($arParams["IMAGE_MAX_HEIGHT"]);
$arParams["ALLOW_POST_CODE"] = $arParams["ALLOW_POST_CODE"] !== "N";

if (!is_array($arParams["POST_PROPERTY_LIST"])) {
    if (CModule::IncludeModule("webdav") || CModule::IncludeModule("disk"))
        $arParams["POST_PROPERTY_LIST"] = array("UF_BLOG_POST_FILE");
    else
        $arParams["POST_PROPERTY_LIST"] = array("UF_BLOG_POST_DOC");
}
else {
    if (CModule::IncludeModule("webdav") || CModule::IncludeModule("disk"))
        $arParams["POST_PROPERTY_LIST"][] = "UF_BLOG_POST_FILE";
    else
        $arParams["POST_PROPERTY_LIST"][] = "UF_BLOG_POST_DOC";
}

$UserGroupID = Array(1);
if ($USER->IsAuthorized())
    $UserGroupID[] = 2;

if ($arParams["SET_TITLE"] == "Y")
    $APPLICATION->SetTitle(GetMessage("BNPL_TITLE"));

$user_id = IntVal($USER->GetID());
$cache = new CPHPCache;
$arParams['USER_ID'] = cUser::getId();
$cache_id = "blog_last_messages_" . serialize($arParams) . "_" . serialize($UserGroupID) . "_" . $USER->IsAdmin() . "_" . CDBResult::NavStringForCache($arParams["BLOG_COUNT"]);
if (($tzOffset = CTimeZone::GetOffset()) <> 0)
    $cache_id .= "_" . $tzOffset;
if ($arParams["USE_SOCNET"] == "Y")
    $cache_id .= "_" . $user_id;
$cache_path = "/" . SITE_ID . "/blog/last_messages_list/";

if ($arParams["CACHE_TIME"] > 0 && $cache->InitCache($arParams["CACHE_TIME"], $cache_id, $cache_path)) {
    $Vars = $cache->GetVars();
    foreach ($Vars["arResult"] as $k => $v)
        $arResult[$k] = $v;
    CBitrixComponentTemplate::ApplyCachedData($Vars["templateCachedData"]);
    $cache->Output();
} else {
    if ($arParams["CACHE_TIME"] > 0)
        $cache->StartDataCache($arParams["CACHE_TIME"], $cache_id, $cache_path);

    $arFilter = Array(
        "<=DATE_PUBLISH" => ConvertTimeStamp(time() + $tzOffset, "FULL", false),
        "PUBLISH_STATUS" => BLOG_PUBLISH_STATUS_PUBLISH,
        "BLOG_ACTIVE" => "Y",
        "BLOG_GROUP_SITE_ID" => SITE_ID,
        ">PERMS" => BLOG_PERMS_DENY
    );
    if (strlen($arParams["BLOG_URL"]) > 0)
        $arFilter["BLOG_URL"] = $arParams["BLOG_URL"];
    if (!empty($arParams["GROUP_ID"]))
        $arFilter["BLOG_GROUP_ID"] = $arParams["GROUP_ID"];
    if ($USER->IsAdmin())
        unset($arFilter[">PERMS"]);

    $arSelectedFields = array(
        "ID",
        "BLOG_ID",
        "TITLE",
        "CODE",
        "AUTHOR_ID",
        "PREVIEW_TEXT",
        "PREVIEW_TEXT_TYPE",
    );

    $SORT = Array($arParams["SORT_BY1"] => $arParams["SORT_ORDER1"], $arParams["SORT_BY2"] => $arParams["SORT_ORDER2"]);

    if ($arParams["MESSAGE_PER_PAGE"])
        $COUNT = array("nPageSize" => $arParams["MESSAGE_PER_PAGE"], "bShowAll" => false);
    else
        $COUNT = false;

    if ($arParams['FILTER']) {
        $arFilter = array_merge($arFilter, $arParams['FILTER']);
    }

    $arResult = Array();
    $dbPosts = CBlogPost::GetList(
                    $SORT, $arFilter, false, $COUNT, $arSelectedFields
    );
    $arResult["NAV_STRING"] = $dbPosts->GetPageNavString(GetMessage("B_B_GR_TITLE"), $arParams["NAV_TEMPLATE"], false, $component);
    $arResult["POSTS"] = Array();

    $p = new blogTextParser(false, $arParams["PATH_TO_SMILE"]);
    $arParserParams = Array(
        "imageWidth" => $arParams["IMAGE_MAX_WIDTH"],
        "imageHeight" => $arParams["IMAGE_MAX_HEIGHT"],
    );

    while ($arPost = $dbPosts->GetNext()) {
        if ($arPost["PREVIEW_TEXT_TYPE"] == "html" && COption::GetOptionString("blog", "allow_html", "N") == "Y") {
            $arAllow = array("HTML" => "Y", "ANCHOR" => "Y", "IMG" => "Y", "SMILES" => "Y", "NL2BR" => "N", "VIDEO" => "Y", "QUOTE" => "Y", "CODE" => "Y");
            if (COption::GetOptionString("blog", "allow_video", "Y") != "Y")
                $arAllow["VIDEO"] = "N";
            $arPost["TEXT_FORMATED"] = $p->convert($arPost["~PREVIEW_TEXT"], true, $arImages, $arAllow, $arParserParams);
        }
        else {
            $arAllow = array("HTML" => "N", "ANCHOR" => "Y", "BIU" => "Y", "IMG" => "Y", "QUOTE" => "Y", "CODE" => "Y", "FONT" => "Y", "LIST" => "Y", "SMILES" => "Y", "NL2BR" => "N", "VIDEO" => "Y");
            if (COption::GetOptionString("blog", "allow_video", "Y") != "Y")
                $arAllow["VIDEO"] = "N";
            $arPost["TEXT_FORMATED"] = $p->convert($arPost["~PREVIEW_TEXT"], true, $arImages, $arAllow, $arParserParams);
        }
        $arResult["POSTS"][] = $arPost;
    }

    if ($arParams["CACHE_TIME"] > 0)
        $cache->EndDataCache(array("templateCachedData" => $this->GetTemplateCachedData(), "arResult" => $arResult));
}

$this->IncludeComponentTemplate();
?>