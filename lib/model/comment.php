<?php

namespace Project\Comment\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class CommentTable extends DataManager {

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'b_blog_comment';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\IntegerField('BLOG_ID'),
            new Main\Entity\IntegerField('POST_ID'),
            new Main\Entity\IntegerField('AUTHOR_ID'),
            new Main\Entity\StringField('PUBLISH_STATUS'),
        );
    }

}
