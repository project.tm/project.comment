(function (window) {
    'use strict';

    if (window.jsProjectCommentBlogComment) {
        return;
    }

    window.jsProjectCommentBlogComment = function (arParams) {
        let self = this;
        self.config = {
            ajax: arParams.AJAX,
            contaner: arParams.CONTANER,
            contanerList: arParams.CONTANER_LIST,
            contanerMore: arParams.CONTANER_MORE,
        };
        self.param = {
            TEMPLATE_NAME: arParams.TEMPLATE_NAME,
            POST_ID: arParams.POST_ID,
            PAGEN: 1
        };

        $(document).on('click', self.config.contanerMore, function () {
            self.param.PAGEN++;
            self.param.IS_UPDATE = 0;
            self.loadAjax(this);
        });
    };

    window.jsProjectCommentBlogComment.prototype.update = function (el) {
        self.param.IS_UPDATE = 1;
        this.loadAjax(el);
    };

    window.jsProjectCommentBlogComment.prototype.loadAjax = function (el) {
        let self = this;
        let load = new projectAjaxLoader(el);
        $.get(self.config.ajax, self.param, function (data) {
            if (data.success) {
                if (self.param.IS_UPDATE) {
                    $(self.config.contaner).html(data.content);
                } else {
                    $(self.config.contanerList).append(data.content);
                }
                load.success(data.isNext);
            } else {
                self.param.PAGEN--;
                load.fail();
            }

        }, 'json').fail(function () {
            self.param.PAGEN--;
            load.fail();
        });
    };

})(window);