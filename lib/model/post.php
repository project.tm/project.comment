<?php

namespace Project\Comment\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class PostTable extends DataManager {

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'b_blog_post';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\IntegerField('BLOG_ID'),
            new Main\Entity\IntegerField('AUTHOR_ID'),
            new Main\Entity\StringField('PUBLISH_STATUS'),
        );
    }

}
