<?php

namespace Project\Comment;

use cUser,
    CDBResult,
    CIBlockElement,
    Igromafia\Game,
    Project\Core\Utility,
    Project\Comment\Model\CommentTable;

class Post {

    public static function getList() {
        return Utility::useCache(array(__CLASS__, __FUNCTION__), function() {
                    $arResult = array();
                    $rsData = CommentTable::getList(array(
                                'select' => array('POST_ID'),
                                'filter' => array(
                                    'BLOG_ID' => Config::BLOG_ID,
                                    'AUTHOR_ID' => cUser::getId(),
                                    'PUBLISH_STATUS' => 'P'
                                ),
                    ));
                    $rsData = new CDBResult($rsData);
                    while ($arComment = $rsData->Fetch()) {
                        $arResult[$arComment['POST_ID']] = $arComment['POST_ID'];
                    }

                    $arSelect = array(
                        "ID",
                        "CREATED_BY",
                        "PROPERTY_BLOG_POST_ID",
                    );
                    foreach (array(
                Game\Config::DZHO_IBLOCK,
                Game\Config::ARTICLE_IBLOCK,
                Game\Config::NEWS_IBLOCK
                    ) as $value) {
                        $arFilter = array(
                            'IBLOCK_ID' => $value,
                            'ACTIVE' => 'Y',
                            '!PROPERTY_BLOG_POST_ID' => false,
                            'CREATED_BY' => cUser::getId(),
                        );
                        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
                        while ($arComment = $res->Fetch()) {
                            $arResult[$arComment['PROPERTY_BLOG_POST_ID_VALUE']] = $arComment['PROPERTY_BLOG_POST_ID_VALUE'];
                        }
                    }
                    return $arResult;
                }, Utility::CACHE_MINUTE);
    }

}
