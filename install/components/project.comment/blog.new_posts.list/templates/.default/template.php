<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if ($arResult["POSTS"]) { ?>
    <? foreach ($arResult["POSTS"] as $ind => $CurPost) { ?>
        <div class="conteiner-block messages-block toggle-js">
            <div class="top-line">
                <a class="section <?= $CurPost['TYPE']['class'] ?>"><?= $CurPost['TYPE']['name'] ?></a><?= $CurPost["TEXT_FORMATED"] ?>
            </div>
            <?
            $APPLICATION->IncludeComponent("project.comment:blog.comment", '.default', array(
                'POST_ID' => $CurPost['ID']
            ));
            ?>
        </div>
    <? } ?>
    <?=$arResult["NAV_STRING"] ?>
<? } else { ?>
    <?= $arParams['NOT_FOUNT'] ?>
<? } ?>