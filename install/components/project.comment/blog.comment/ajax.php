<?

use Bitrix\Main\Application;

define('FAVORIT_AJAX', true);
define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC', 'Y');
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$arResult = array();
ob_start();
$request = Application::getInstance()->getContext()->getRequest();
$arResult['success'] = true;
$arResult['isNext'] = $APPLICATION->IncludeComponent('project.comment:blog.comment', $request->get('TEMPLATE_NAME'), Array(
    'IS_AJAX' => 'Y',
    'POST_ID' => $request->get('POST_ID'),
    'PAGEN' => $request->get('PAGEN'),
    'IS_UPDATE' => $request->get('IS_UPDATE')
        ));
$arResult['content'] = ob_get_clean();
echo json_encode($arResult);
